package com.nw.blog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nw.blog.models.Post;
import com.nw.blog.services.NotificationService;
import com.nw.blog.services.PostService;

@Controller
public class PostController {

	@Autowired
	PostService postService;
	
	@Autowired
	NotificationService notificationService;
	
	@RequestMapping("posts/view/{id}")
	public String view(@PathVariable("id") Long id, Model model){
		Post post = postService.findById(id);
		if(post == null){
			notificationService.addErrorMessage("Cannot find post #" + id);
//			return "redirect:/";
		}
		model.addAttribute("post", post);
		return "posts/view";
	}
	
}
