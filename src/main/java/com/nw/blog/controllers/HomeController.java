package com.nw.blog.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nw.blog.models.Post;
import com.nw.blog.services.PostService;

@Controller
public class HomeController {
	
	@Autowired
	PostService postService;
	
	@RequestMapping("/")
	public String index(Model model){
		List<Post> latest5Post = postService.findLatest5();
		model.addAttribute("latest5posts", latest5Post);
		List<Post> latest3Post = latest5Post.stream().limit(3).collect(Collectors.toList());
		model.addAttribute("latest3posts", latest3Post);
		return "index";
	}
	
}
